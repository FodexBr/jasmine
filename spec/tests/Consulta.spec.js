describe("ConsultaTest", () => {

	let Pessoa;
	let PessoaBuilder;
	let Consulta;
	let paciente;

	beforeEach(() => {
		Pessoa = require("../../lib/Paciente.js");
		// Test Data Builder: Implemntação de Patterns Builders para Tests TDD
		PessoaBuilder = require('../../lib/PacienteBuilder.js');
		Consulta = require("../../lib/Consulta.js");

		paciente = new PessoaBuilder().setNome("Júlia").build();
	});

	// Describes dentro de Describes para organizar a bateria de testes
	describe("Consultas do Tipo Retorno", () => {

		it("Não deve cobrar nada se for umr retorno", () => {
			const consulta = new Consulta(paciente, [],true,true);
			expect(consulta.preco()).toEqual(0);
		});
		
	});


	describe("Consultas com Procedimentos", () => {
		it("Deve cobrar 25 reais por cada procedimento comum", () => {
			const consulta = new Consulta(paciente, ['proc1', 'proc2'],false,false);
			expect(consulta.preco()).toEqual(50);
		});
		
		it("Deve testar consulta particular com procedimentos comums", () => {
			const consulta = new Consulta(paciente, ['proc1', 'proc2'],true,false);
			expect(consulta.preco()).toEqual(50 * 2);
		});
		
		it("Deve testar consulta particular com procedimentos especiais", () => {
			const consulta = new Consulta(paciente, ['raio-x', 'proc2'],true,false);
			expect(consulta.preco()).toEqual(75 * 2);
		});

		it("Deve cobrar preco especifico dependendo do procedimento ", () => {
			const consulta = new Consulta(paciente, ['gesso', 'proc2'],false,false);
			expect(consulta.preco()).toEqual(105);
		});
	})


});