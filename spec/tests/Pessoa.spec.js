describe("PessoaTest", () => {

	// Roda o beforeEach antes de cada Teste
	var paciente;
	var Paciente;
	beforeEach(function() {
		paciente = require('../../lib/Paciente.js');
		Paciente = new paciente("Pedro", 19, 70, 1.84);
	});


	// Classes de equivalencia : são classes que sempre tem a mesma forma de xecucao, passam pelo mesmo algortimo
	// Classes de equivalência é o nome que damos para quando temos testes "parecidos", que exercitam o mesmo caminho no código de produção.
	
	it("deve calcular o imc da pessoa", () => {
		expect(Paciente.peso / (Math.pow(Paciente.altura))).toEqual(Paciente.imc());
	});

	it("deve calcular o total de batimentos da vida de um paciente", () => {
		expect(Paciente.batimentos()).toEqual(798912000);
	});

})