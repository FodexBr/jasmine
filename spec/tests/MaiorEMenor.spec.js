//Descreve o Cenario, ou seja , 'quem eu vou testar'
describe("TesteMaioreMenor", function() {

	const MaiorEManor = require('../../lib/MaiorEMenor.js');


	// Explicacao do teste, eu vou tester o que dentro de "quem eu vou testar"
	it("deve entender numeros em ordem sequencial", function() {
		let alg = new MaiorEManor();
		alg.encontra(5,17,9,2);

		expect(alg.maior).toEqual(17);
		expect(alg.menor).toEqual(2);
	});


	it("deve entender numeros em ordems crescente", function() {
		let alg = new MaiorEManor();
		alg.encontra(5,6,7,8);

		expect(alg.maior).toEqual(8);
		expect(alg.menor).toEqual(5);
	});

	it("deve entender numeros em ordem descrescente", function() {
		let alg = new MaiorEManor();
		alg.encontra(8,7,6,5);

		expect(alg.maior).toEqual(8);
		expect(alg.menor).toEqual(5);
	});

	it("teste de apenas um numero", function() {
		let alg = new MaiorEManor();
		alg.encontra(8);

		expect(alg.maior).toEqual(8);
		expect(alg.menor).toEqual(8);

	});


}); 