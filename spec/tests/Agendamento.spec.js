describe("Agendamento",() => {

	let barrel = require('../../lib/index.js'); 	

	let consulta;	
	let paciente;
	let agendamento;

	beforeEach(() => {
		consulta = new barrel.Consulta(paciente, [], false, false, new Date(2014, 7, 1));
		agendamento = new barrel.Agendamento();
		paciente  = new barrel.PacienteBuilder().build();
		
	}) ;


	it("Deve agendar para 20 dias depois", () => {
		let novaConsulta  = agendamento.para(consulta);
		expect(novaConsulta.data.toString()).toEqual(new Date(2014, 7, 21).toString());

	});

	it("Deve pular fins de semana", function() {
		consulta.data = new Date(2014, 5, 30);
		let novaConsulta = agendamento.para(consulta);
		expect(novaConsulta.data.toString()).toEqual(new Date(2014, 6, 21).toString());
	})


});