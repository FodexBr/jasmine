** Escrevendo um Teste

	## Os arquivos .js dos codigos/algoritmos do sistema
	devem  ficar em qualquer pasta, porem devemos dar um 
	"module.exports" do objeto javascript

	## Os arquivos de test devem ficar em spec/meuteste.spec.js


	## Programando um Teste
	describe("Descrevo o cenario do teste", () => {

		it("Descrevo qual deve ser o teste para esse cenário", () => {

			... lógica do teste

			expect(n).toEquals(n);
		})
	});