module.exports = class Consulta {

	constructor(_paciente, _procedimentos, _particular, _retorno, _data) {
		this._paciente = _paciente;
		this._procedimentos = _procedimentos;
		this._particular = _particular;
		this._retorno = _retorno;
		this._data = _data;
	}

	preco() {
		if(this._retorno) {
			return 0;
		}

		let precoFinal = 0;

		this._procedimentos.forEach(procedimento => {
			if(procedimento == 'raio-x') precoFinal+= 50;
			else if(procedimento == 'gesso') precoFinal+= 80;
			else  precoFinal +=  25; 
		});

		if(this._particular) {
			return precoFinal *= 2;
		}

		return precoFinal;
	}

	get paciente() { return this._paciente; }
	get procedimentos(){ return this._procedimentos;}
	get particular(){ return this._particular; }
	get retorno(){ return this._retorno; }
	get data() { return this._data; }

	set paciente(param) {  this._paciente = param; }
	set procedimentos(param){  this._procedimento = param;}
	set particular(param){  this._particular = param; }
	set retorno(param){  this._retorno = param; }
	set data(param) {  this._data = param; }

}