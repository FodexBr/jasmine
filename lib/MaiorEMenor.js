function MaiorEManor() {
	let maior = "";
	let menor = "";

	var clazz = {

		encontra(...numeros) {
			maior = Number.MIN_VALUE;
	 		menor = Number.MAX_VALUE;

	 		
			numeros.forEach(num => {
				if(num < menor){
					menor = num; 
				} 
				if(num > maior) {
					maior = num;
				}
			});
		},

		get maior() { return maior; }, 
		get menor() { return menor; }
	}

	return clazz;

}


module.exports = MaiorEManor;