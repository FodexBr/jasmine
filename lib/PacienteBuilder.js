const Pessoa = require("./Paciente.js");
module.exports =  class PacienteBuilder {

	constructor() {
		this._nome = "Pedro";
		this._idade = 19;
		this._peso = 70;
		this._altura=  1.84;
	}


	setNome(nome) {
		this._nome = nome;
		return this;
	}

	setIdade(idade) {
		this._idade = idade;
		return this;
	}

	setPeso(peso) {
		this._peso = peso;
		return this;
	}


	setAltura(altura) {
		this._altura = altura;
		return this;
	}


	build() {
		return new Pessoa(this._nome, this._idade, this._peso, this._altura);
	}
}