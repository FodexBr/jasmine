const Consulta = require('./Consulta.js');
module.exports = class Agendamento {

	constructor() {

	}

	para(consulta) {
		const umDiaEmMilisegundos = (1000 * 60 * 60 * 24 * 1);
		const vinteDiasEmMiliSegundos = (umDiaEmMilisegundos * 20);

		let novaData = new Date(consulta.data.getTime() + vinteDiasEmMiliSegundos);

		while(novaData.getDay() == 0 || novaData.getDay() == 6) {
			novaData = new Date(novaData.getTime() + umDiaEmMilisegundos);
		}

		return new Consulta(consulta.nome,consulta.procedimentos, consulta.particular, true, novaData);
	}

}