let MaiorEMenor = require('./MaiorEMenor.js');
let PacienteBuilder = require('./PacienteBuilder.js');
let Paciente = require('./Paciente.js');
let Consulta = require('./Consulta.js');
let Agendamento = require('./Agendamento.js');

module.exports = {
	PacienteBuilder,
	Paciente,
	Consulta,
	Agendamento,
	MaiorEMenor
};