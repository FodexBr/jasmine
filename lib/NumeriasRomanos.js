module.exports = class NumeraisRomanos {

	constructor(roman) {
		this._roman = roman;
	}

	get num(){
		let num = 0;

		switch(this._roman) {
			case 'I':
				num = 1;
				break;

			case 'II':
				num = 2;
				break;

			case 'III':
				num = 3;
				break;

			case 'IV':
				num = 4;
				break;

			case 'V':
				num = 5;
				break;

			case 'X':
				num = 10;
				break;
		}


		return num;



	}

}